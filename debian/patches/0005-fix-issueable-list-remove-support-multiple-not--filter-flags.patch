Description: fix(issueable list): remove support multiple not- filter flags
 This change set fixes the issueable list command line interface by
 removing support for providing multiple `--not-assignee` and
 `not-author` flags. Those two flags have never functioned the way it was
 intended, instead they silently failed by only considering the first
 passed value to the API. All other values have been ignored.
 .
 This change set therefore changes so that the `--not-assignee` and
 `--not-author` flags can only submitted once.
 .
 This is a breaking change to the CLI justified by the feature never
 working as intended without providing any information to the user.
 .
 An alternative fix would submit the API request multiple times and merge
 the results.
Author: Timo Furrer <tfurrer@gitlab.com>
Origin: upstream, commit:6b3ce31b1687571ff9cf6b2f121dd9e430684302
Forwarded: not-needed
Reviewed-By: Nicolas Schier <nicolas@fjasle.eu>
Last-Update: 2025-02-25

--- glab-1.52.0.orig/commands/issuable/list/issuable_list.go
+++ glab-1.52.0/commands/issuable/list/issuable_list.go
@@ -24,9 +24,9 @@ import (
 type ListOptions struct {
 	// metadata
 	Assignee    string
-	NotAssignee []string
+	NotAssignee string
 	Author      string
-	NotAuthor   []string
+	NotAuthor   string
 	Labels      []string
 	NotLabels   []string
 	Milestone   string
@@ -129,9 +129,9 @@ func NewCmdList(f *cmdutils.Factory, run
 	}
 	cmdutils.EnableRepoOverride(issueListCmd, f)
 	issueListCmd.Flags().StringVarP(&opts.Assignee, "assignee", "a", "", fmt.Sprintf("Filter %s by assignee <username>.", issueType))
-	issueListCmd.Flags().StringSliceVar(&opts.NotAssignee, "not-assignee", []string{}, fmt.Sprintf("Filter %s by not being assigneed to <username>.", issueType))
+	issueListCmd.Flags().StringVar(&opts.NotAssignee, "not-assignee", "", fmt.Sprintf("Filter %s by not being assigned to <username>.", issueType))
 	issueListCmd.Flags().StringVar(&opts.Author, "author", "", fmt.Sprintf("Filter %s by author <username>.", issueType))
-	issueListCmd.Flags().StringSliceVar(&opts.NotAuthor, "not-author", []string{}, "Filter by not being by author(s) <username>.")
+	issueListCmd.Flags().StringVar(&opts.NotAuthor, "not-author", "", fmt.Sprintf("Filter %s by not being by author(s) <username>.", issueType))
 	issueListCmd.Flags().StringVar(&opts.Search, "search", "", "Search <string> in the fields defined by '--in'.")
 	issueListCmd.Flags().StringVar(&opts.In, "in", "title,description", "search in: title, description.")
 	issueListCmd.Flags().StringSliceVarP(&opts.Labels, "label", "l", []string{}, fmt.Sprintf("Filter %s by label <name>.", issueType))
@@ -191,12 +191,12 @@ func listRun(opts *ListOptions) error {
 		}
 		listOpts.AssigneeUsername = gitlab.Ptr(opts.Assignee)
 	}
-	if len(opts.NotAssignee) != 0 {
-		u, err := api.UsersByNames(apiClient, opts.NotAssignee)
+	if opts.NotAssignee != "" {
+		u, err := api.UserByName(apiClient, opts.NotAssignee)
 		if err != nil {
 			return err
 		}
-		listOpts.NotAssigneeID = cmdutils.IDsFromUsers(u)
+		listOpts.NotAssigneeID = gitlab.Ptr(u.ID)
 	}
 	if opts.Author != "" {
 		u, err := api.UserByName(apiClient, opts.Author)
@@ -205,12 +205,12 @@ func listRun(opts *ListOptions) error {
 		}
 		listOpts.AuthorID = gitlab.Ptr(u.ID)
 	}
-	if len(opts.NotAuthor) != 0 {
-		u, err := api.UsersByNames(apiClient, opts.NotAuthor)
+	if opts.NotAuthor != "" {
+		u, err := api.UserByName(apiClient, opts.NotAuthor)
 		if err != nil {
 			return err
 		}
-		listOpts.NotAuthorID = cmdutils.IDsFromUsers(u)
+		listOpts.NotAuthorID = gitlab.Ptr(u.ID)
 	}
 	if opts.Search != "" {
 		listOpts.Search = gitlab.Ptr(opts.Search)
--- glab-1.52.0.orig/docs/source/incident/list.md
+++ glab-1.52.0/docs/source/incident/list.md
@@ -45,8 +45,8 @@ glab incident list --milestone release-2
       --in string              search in: title, description. (default "title,description")
   -l, --label strings          Filter incident by label <name>.
   -m, --milestone string       Filter incident by milestone <id>.
-      --not-assignee strings   Filter incident by not being assigneed to <username>.
-      --not-author strings     Filter by not being by author(s) <username>.
+      --not-assignee string    Filter incident by not being assigned to <username>.
+      --not-author string      Filter incident by not being by author(s) <username>.
       --not-label strings      Filter incident by lack of label <name>.
   -O, --output string          Options: 'text' or 'json'. (default "text")
   -F, --output-format string   Options: 'details', 'ids', 'urls'. (default "details")
--- glab-1.52.0.orig/docs/source/issue/list.md
+++ glab-1.52.0/docs/source/issue/list.md
@@ -47,8 +47,8 @@ glab issue list --milestone release-2.0.
   -i, --iteration int          Filter issue by iteration <id>.
   -l, --label strings          Filter issue by label <name>.
   -m, --milestone string       Filter issue by milestone <id>.
-      --not-assignee strings   Filter issue by not being assigneed to <username>.
-      --not-author strings     Filter by not being by author(s) <username>.
+      --not-assignee string    Filter issue by not being assigned to <username>.
+      --not-author string      Filter issue by not being by author(s) <username>.
       --not-label strings      Filter issue by lack of label <name>.
   -O, --output string          Options: 'text' or 'json'. (default "text")
   -F, --output-format string   Options: 'details', 'ids', 'urls'. (default "details")
